package com.mindex.challenge.service.impl;

import com.mindex.challenge.dao.EmployeeRepository;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Autowired
    ReportingStructure reportingStructure; 

    @Override
    public Employee create(Employee employee) {
        LOG.debug("Creating employee [{}]", employee);

        employee.setEmployeeId(UUID.randomUUID().toString());
        employeeRepository.insert(employee);

        return employee;
    }

    @Override
    public Employee read(String id) {
        LOG.debug("Creating employee with id [{}]", id);

        Employee employee = employeeRepository.findByEmployeeId(id);

        if (employee == null) {
            throw new RuntimeException("Invalid employeeId: " + id);
        }

        return employee;
    }

    @Override
    public Employee update(Employee employee) {
        LOG.debug("Updating employee [{}]", employee);

        return employeeRepository.save(employee);
    }

    @Override	
    public ReportingStructure getNumberOfReportsForEmployee(String id) {
	LOG.debug("Find number of direct  employees [{}]", id);

       Employee employee = employeeRepository.findByEmployeeId(id);

       if (employee == null) {
          throw new RuntimeException("Invalid employeeId: " + id);
       }

	// find directReports of employee
		List<Employee> reportList = employee.getDirectReports();

		int numberOfReports = 0;

		if (reportList.size() > 0) {
			numberOfReports = reportList.size();
			for (Employee emp : reportList) {
				int totalNoReportees = getNumberOfReportees(emp.getEmployeeId());
				numberOfReports += totalNoReportees;
			}
		}

		reportingStructure.setEmployee(employee);
		reportingStructure.setNumberOfReports(numberOfReports);

		return reportingStructure;

	}

	public int getNumberOfReportees(String empID) {
		Employee dirReportEmp = employeeRepository.findByEmployeeId(empID);
		List<Employee> reporteeList = dirReportEmp.getDirectReports();
		int reporteesNo =0;
		if (reporteeList.size() > 0) {
			reporteesNo += reporteeList.size();
			for (Employee emp : reporteeList) {
				int totalNoReportees = getNumberOfReportees(emp.getEmployeeId());
				reporteesNo += totalNoReportees;
			}
			return reporteesNo;
		} else {
			return 0;
		      }
		}
	}

	}
