package com.moindex.challenge.data;

import java.util.List;

public class ReportingStructure {
    private int numberOfReports;
    
    @Autowired
    private Employee employee;

    public ReportingStructure() {
    }

    public int getNumberOfReports() {
        return numberOfReports;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
