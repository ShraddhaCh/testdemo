package com.mindex.challenge.data;

import java.util.List;

public class Compensation {
    private int salary;
    
    @Autowired    
    private Employee employee;
    private Date effectiveDate;

    public Compensation() {
    }

    public int getSalary() {
        return salary;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setSalary(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }
}
